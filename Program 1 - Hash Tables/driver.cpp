// Name: Jonathon Beranek
// OOP Program 1
// Driver to test functions in hash.cpp

#include "hash.cpp"
#include <iostream>

//hash functions to be passed into each table initialization
struct stringHash {

	int operator()(std::string key) {
		std::size_t hash = std::hash<std::string>()(key);
		return hash;
	}
};

struct intHash {

	int operator()(int key) {
		return (key / 2 + key + 2) * 12;
	}
};

struct floatHash {
	
	int operator()(float key) {
		return sqrt((key*key) + 5) * 91;
	}
};

int main() {
	
	int max, init;

	//asks user for bounds for the program to know how to build each table

	std::cout << "Enter max load factor: ";
	std::cin >> max;

	std::cout << "Enter initial size: ";
	std::cin >> init;

	HashTable<std::string, int, stringHash> sTable(max, init);
	HashTable<int, int, intHash> iTable(max, init);
	HashTable<float, double, floatHash> dTable(max, init);

	//each block of code below is adding generic info into each HashTable
	
	int iPlaceHolder = NULL;
	double dPlaceHolder = NULL;
	int searchTestVal = 334;

	//string table setup
	sTable.insert("cat", 5);
	sTable.insert("dog", 4);
	sTable.insert("god", 88);
	sTable.insert("doge", 22);
	sTable.insert("cat", 13);
	sTable.insert("apples", 19);
	sTable.insert("kit", 22);
	sTable.insert("pocket", 66);
	sTable.insert("tack", 147);
	sTable.insert("lock", searchTestVal);


	//int table setup
	iTable.insert(2, 5);
	iTable.insert(9, 4);
	iTable.insert(12, 88);
	iTable.insert(15, 22);
	iTable.insert(72, 13);
	iTable.insert(61, 19);
	iTable.insert(121, 22);
	iTable.insert(32, 66);
	iTable.insert(3, 147);
	iTable.insert(56, searchTestVal);

	//float table setup
	dTable.insert(1.23f, 5);
	dTable.insert(4.2341f, 4.7);
	dTable.insert(607.001f, 88.99);
	dTable.insert(4.00f, 22);
	dTable.insert(300.1f, 13);
	dTable.insert(9999.9999f, 19.12);
	dTable.insert(21.56f, 22);
	dTable.insert(666.666f, 66);
	dTable.insert(23.0f, 147.26);
	dTable.insert(91.0005234f, searchTestVal);


	//prints out true if the values are there; all should be valid except for last value; also prints out element value grabbed in first print out

	//string testing
	std::cout << sTable.search("cat", iPlaceHolder) << " " << iPlaceHolder << std::endl;
	std::cout << sTable.search("dog", iPlaceHolder) << std::endl;
	std::cout << sTable.search("god", iPlaceHolder) << std::endl;
	std::cout << sTable.search("doge", iPlaceHolder) << std::endl;
	std::cout << sTable.search("cat", iPlaceHolder) << std::endl;
	std::cout << sTable.search("apples", iPlaceHolder) << std::endl;
	std::cout << sTable.search("kit", iPlaceHolder) << std::endl;
	std::cout << sTable.search("pocket", iPlaceHolder) << std::endl;
	std::cout << sTable.search("tack", iPlaceHolder) << std::endl;
	std::cout << sTable.search("lock", iPlaceHolder) << std::endl;
	std::cout << sTable.search("luck", iPlaceHolder) << std::endl << std::endl;

	// if last test is false, iPlaceHolder should be the same as the last value (which will be searchTestVal)
	if (iPlaceHolder == searchTestVal) {
		std::cout << "Search has worked for string: " << iPlaceHolder << "\n\n";
	}


	//int testing
	std::cout << iTable.search(2, iPlaceHolder) << " " << iPlaceHolder << std::endl;
	std::cout << iTable.search(9, iPlaceHolder) << std::endl;
	std::cout << iTable.search(12, iPlaceHolder) << std::endl;
	std::cout << iTable.search(15, iPlaceHolder) << std::endl;
	std::cout << iTable.search(72, iPlaceHolder) << std::endl;
	std::cout << iTable.search(61, iPlaceHolder) << std::endl;
	std::cout << iTable.search(121, iPlaceHolder) << std::endl;
	std::cout << iTable.search(32, iPlaceHolder) << std::endl;
	std::cout << iTable.search(3, iPlaceHolder) << std::endl;
	std::cout << iTable.search(56, iPlaceHolder) << std::endl;
	std::cout << iTable.search(1024, iPlaceHolder) << std::endl << std::endl;

	if (iPlaceHolder == searchTestVal) {
		std::cout << "Search has worked for int: " << iPlaceHolder << "\n\n";
	}


	// float testing
	std::cout << dTable.search(1.23f, dPlaceHolder) << " " << dPlaceHolder << std::endl;
	std::cout << dTable.search(4.2341f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(607.001f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(4.00f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(300.1f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(9999.9999f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(21.56f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(666.666f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(23.0f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(91.0005234f, dPlaceHolder) << std::endl;
	std::cout << dTable.search(666.0f, dPlaceHolder) << std::endl << std::endl;

	if (dPlaceHolder == searchTestVal) {
		std::cout << "Search has worked for float: " << dPlaceHolder << "\n\n";
	}
	

	// code to end program
	std::cout << "Tests complete! Type anything and hit enter to close...\n";
	int temp;
	std::cin >> temp;

	return 0;
};
// Name: Jonathon Beranek
// OOP Program 1
// File which declares and defines the HashTable template class 

#include <vector>
#include <list>
#include <iostream>

template<typename K, typename E, typename UNICORN>
class HashTable {
public:
	//obligatory default constructor for the case where the program doesn't ask for load size
	HashTable() {
		max_factor = 5;
		items = 0;
		table.resize(5);
	}
	//common-use contructor for nomal needs of using the hash table
	HashTable(const int& factor, const int& i_cap) {
		maxFactor = factor;
		items = 0;
		table.resize(i_cap);
	}

	//searches to see if a certain key exists and get it's associated value if so
	bool search(const K& key, E& toReturn);

	//inserts a key and value pair
	void insert(const K& key, const E& elem);

private:
	void regrow();	//regrow is private as it should only ever be accessed by the class itself

	std::vector<std::list< std::pair<K, E> >> table;
	int maxFactor;
	int items;
	UNICORN function;
};

template<typename K, typename E, typename UNICORN>
void HashTable<K, E, UNICORN>::regrow() {
	std::vector<std::list< std::pair<K, E> >> newTable;
	newTable.resize(table.size() * 2);
	for (int i = 0; i < table.size(); i++) {
		for (auto iter = table.at(i).cbegin(); iter != table.at(i).cend(); ++iter) {
			//this line is basically using the hash function and table size to go the appropriate spot in the new, bigger vector, and adding the previous table's info in there
			newTable.at(function((*iter).first) % newTable.size()).push_back(std::pair<K, E>(*iter));
		}
	}
	table = newTable; //point table to newTable
}

// works around a need for exceptions and instead gives the end-programmer the choice with what to do with his returned info
template<typename K, typename E, typename UNICORN>
bool HashTable<K, E, UNICORN>::search(const K& key, E& toReturn) {
	int hashedKey = function(key) % table.size();
	for (auto iter = table.at(hashedKey).begin(); iter != table.at(hashedKey).end(); ++iter) {
		if ((*iter).first == key) {
			toReturn = (*iter).second;
			return true; // if the key matches, the function will return true, and pass the found value into 'toReturn'
		}
	}
	return false; // if no key is found, the function will return false, and 'toReturn' will keep its value, this working around the need for exceptions
}

template<typename K, typename E, typename UNICORN>
void HashTable<K, E, UNICORN>::insert(const K& key, const E& elem) {
	items++;
	//based off of load factor, checks to see if table needs regrowth before continuing
	if ((items / table.size()) >= maxFactor) {
		regrow();
	}
	// hashes given key and mods it according to table size to find an appropriate spot in the vector; it then adds in the pair of the key and element given
	table.at(function(key) % table.size()).push_back(std::pair<K, E>(key, elem));
}